package com.android.rohan.vtr;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class MainActivity extends AppCompatActivity implements CvCameraViewListener{

    private CameraBridgeViewBase mOpenCvCameraView;
    private CascadeClassifier cascadeClassifier;
    private int absoluteFaceSize;
    private Mat tshirt;
    private int pos_x, pos_y;
    private Size tshirt_size;
    private Mat curr_frame;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    InitializeOpenCVDependencies();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    private void InitializeOpenCVDependencies() {
        try {
            InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
            File cascadeDir = getDir("cascade", Context.MODE_APPEND);
            File mCascadeFile = new File(cascadeDir, "lbpcascade_frontalface.xml");
            FileOutputStream os = new FileOutputStream(mCascadeFile);

            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead=is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();

            cascadeClassifier = new CascadeClassifier(mCascadeFile.getAbsolutePath());

        } catch (Exception e) {
            Log.e("OpenCV Activity", "Error Loading Haarcascade", e);
        }
        mOpenCvCameraView.enableView();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
       return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.tshirt1:
                setTshirt(R.drawable.tshirt1);
                return true;
            case R.id.tshirt2:
                setTshirt(R.drawable.tshirt2);
                return true;
            case R.id.tshirt3:
                setTshirt(R.drawable.tshirt3);
                return true;
            case R.id.tshirt4:
                setTshirt(R.drawable.tshirt4);
                return true;
            case R.id.tshirt5:
                setTshirt(R.drawable.tshirt5);
                return true;
            case R.id.tshirt6:
                setTshirt(R.drawable.tshirt6);
                return true;
            case R.id.size_s:
                tshirt_size = new Size(380, 380);
                Imgproc.resize(tshirt, tshirt, tshirt_size);
                return true;
            case R.id.size_m:
                tshirt_size = new Size(390, 390);
                Imgproc.resize(tshirt, tshirt, tshirt_size);
                return true;
            case R.id.size_l:
                tshirt_size = new Size(410, 410);
                Imgproc.resize(tshirt, tshirt, tshirt_size);
                return true;
            case R.id.size_xl:
                tshirt_size = new Size(430, 430);
                Imgproc.resize(tshirt, tshirt, tshirt_size);
                return true;
            case R.id.size_xxl:
                tshirt_size = new Size(460, 460);
                Imgproc.resize(tshirt, tshirt, tshirt_size);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.opencv_activity_surface_view);

        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);

        Button save_btn = (Button) findViewById(R.id.button);
        save_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i("Custom", "clicked");

                File filepath = Environment.getExternalStorageDirectory();
                File dir = new File(filepath.getAbsolutePath()
                        + "/VTR/");
                dir.mkdirs();
                Bitmap bitmap  = Bitmap.createBitmap(curr_frame.width(), curr_frame.height(), Bitmap.Config.RGB_565);

                Utils.matToBitmap(curr_frame, bitmap);

                Calendar c = Calendar.getInstance();


                String filename = "VTR-" + c.hashCode() + ".jpg";
                Log.i("Custom", filename);
                File file = new File(dir, filename);

                Toast.makeText(MainActivity.this, "Image Saved", Toast.LENGTH_SHORT).show();

                try {
                    FileOutputStream output = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, output);
                    Log.i("Custom", "Worked");
                    output.close();
                    Log.i("Custom", "Worked");
                }
                catch (Exception e) {
                    e.printStackTrace();
                    Log.i("Custom", "Caught");
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!OpenCVLoader.initDebug()) {
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_2_0, this, mLoaderCallback);
        } else {
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mOpenCvCameraView != null) {
            mOpenCvCameraView.disableView();
        }
    }


    protected void setTshirt(int tshirt_id) {
        Bitmap mBitmap = BitmapFactory.decodeResource(getResources(), tshirt_id);
        Utils.bitmapToMat(mBitmap, tshirt);
        Imgproc.resize(tshirt, tshirt, tshirt_size);
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        absoluteFaceSize = (int) (height * 0.1);

        tshirt = new Mat();
        curr_frame = new Mat();
        tshirt_size = new Size(380, 380);
        setTshirt(R.drawable.tshirt1);

        pos_x = 0;
        pos_y = 0;
    }

    @Override
    public void onCameraViewStopped() {
       tshirt.release();
    }

    public void addCloth(Mat inputFrame, int cx, int cy) {
        Mat crop_tshirt = tshirt.clone();
        Mat picture_bw = new Mat();
        Mat bg_mask = new Mat();
        Boolean extreme = false;

        cx = cx - (crop_tshirt.cols() / 2);
        cy += 10;

        int end_x = cx + crop_tshirt.cols();
        int end_y = cy + crop_tshirt.rows();

        Rect tshirt_roi = new Rect(0, 0, crop_tshirt.cols(), crop_tshirt.rows());

        if(cx < 0) {
            tshirt_roi.x = -cx;
            tshirt_roi.width = crop_tshirt.cols() + cx;
        }

        if(cy < 0) {
            tshirt_roi.y = -cy;
            tshirt_roi.height = crop_tshirt.rows() + cy;
        }

        if(end_x < 0 || end_y < 0) {
            extreme = true;
        }

        if(end_x > inputFrame.cols())
                tshirt_roi.width = inputFrame.cols() - cx;

        if(end_y > inputFrame.rows())
                tshirt_roi.height = inputFrame.rows() - cy;

        if(cx > inputFrame.cols() || cy > inputFrame.rows()) {
            extreme = true;
        }

        if(extreme)
            return;

        crop_tshirt = crop_tshirt.submat(tshirt_roi);

        Imgproc.cvtColor(crop_tshirt, picture_bw, Imgproc.COLOR_RGBA2GRAY);
        Imgproc.threshold(picture_bw, bg_mask, 1, 255, Imgproc.THRESH_BINARY_INV);

        if(cx < 0)
            cx = 0;

        if(cy < 0)
            cy = 0;

        Rect roi = new Rect(cx, cy, crop_tshirt.cols(), crop_tshirt.rows());

        Mat dstROI = inputFrame.submat(roi);
        Mat tempDst = new Mat();
        Core.bitwise_and(dstROI, dstROI, tempDst, bg_mask);
        tempDst.copyTo(dstROI);
        Core.addWeighted(dstROI, 1, crop_tshirt, 1, 0, dstROI);

        tempDst.release();
        crop_tshirt.release();
        picture_bw.release();
        bg_mask.release();
    }

    @Override
    public Mat onCameraFrame(Mat inputFrame) {

        ///////////////////  Face Detection Code

        Mat grayscale =  new Mat();

        Imgproc.cvtColor(inputFrame, grayscale, Imgproc.COLOR_RGBA2GRAY);

        MatOfRect faces = new MatOfRect();

        if(cascadeClassifier != null) {
            cascadeClassifier.detectMultiScale(grayscale, faces, 2, 1, 2, new Size(absoluteFaceSize, absoluteFaceSize), new Size());
        }

        grayscale.release();
        Rect[] facesArray = faces.toArray();
        if(facesArray.length > 0)  {
            Imgproc.rectangle(inputFrame, facesArray[0].tl(), facesArray[0].br(), new Scalar(0, 255, 0, 255), 3);
            int rect_height = (int)(facesArray[0].br().y - facesArray[0].tl().y);
            pos_x = (int)((facesArray[0].br().x + facesArray[0].tl().x)/2);
            pos_y = (int)((facesArray[0].tl().y + facesArray[0].br().y)/2);
            pos_y += (int)2 * rect_height / 3;
        }

        //////////////////////   Tshirt super imposition

        addCloth(inputFrame, pos_x, pos_y);

        inputFrame.copyTo(curr_frame);
        return inputFrame;
    }
}
